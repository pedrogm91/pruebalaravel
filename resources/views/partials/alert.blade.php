{{-- Message --}}
@if (Session::has('success'))
    <p class="text-white">
        <strong class="text-green-300">successful!</strong> {{ session('success') }}
    </p>
@endif

@if (Session::has('error'))
    <p class="text-white">
    <strong class="text-red-300">Error! </strong> {{ session('error') }}
    </p>
@endif