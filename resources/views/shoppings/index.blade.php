<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Products') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">
                    @include('partials.alert')
                    
<div class="overflow-x-auto relative">
    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
            <tr>
                <th scope="col" class="py-3 px-6">
                    name
                </th>
                <th scope="col" class="py-3 px-6">
                    status
                </th>
                <th scope="col" class="py-3 px-6">
                    user
                </th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            
            @forelse ($shoppings as $shopping)
                <tr class="bg-white dark:bg-gray-800">
                    <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                       {{$shopping->name}}
                    </th>
                    <td class="py-4 px-6">
                        {{$shopping->status}}
                    </td>
                    <td class="py-4 px-6">
                        {{$shopping->user->name}}
                    </td>
                    <td>
                        <form method="post" action="{{ route('invoices.store') }}"> 
                            @csrf
                            @method('POST')
                            <input type="hidden" name="id" value="{{$shopping->id}}">
                            <button type="submit">Generar factura</button>
                        </form>
                    </td>
                </tr>
                    
                @empty
                <tr>
                    <td>No hay productos</td>    
                </tr> 
                @endforelse
        </tbody>
    </table>
</div>

                </div>
            </div>
        </div>
    </div>
</x-app-layout>