<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ __('Details invoice') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900 dark:text-gray-100">

                    <div class="overflow-x-auto relative">
                        <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                            <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                                <tr>
                                    <th scope="col" class="py-3 px-6">
                                        Product
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        Price
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        Tax
                                    </th>
                                    <th scope="col" class="py-3 px-6">
                                        subtotal
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($details as $item)
                                    
                                    <tr class="bg-white dark:bg-gray-800">
                                        <th scope="row" class="py-4 px-6 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                                           {{$item['name']}}
                                        </th>
                                        <td class="py-4 px-6">
                                            {{$item['price']}}
                                        </td>
                                        <td class="py-4 px-6">
                                            {{$item['tax']}}
                                        </td>
                                        <td class="py-4 px-6">
                                            {{$item['subtotal']}}
                                        </td>
                                    </tr>
                                @endforeach
                                     
                            </tbody>
                        </table>
                    </div>



                    inpuesto: {{ $invoice->tax_total }} <br>
                    Total: {{ $invoice->total }}

                </div>
            </div>
        </div>
    </div>

</x-app-layout>
  