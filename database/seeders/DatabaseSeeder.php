<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use \App\Models\Product;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Pedro',
            'email' => 'pedrogm91@gmail.com',
            'role' => 'admin',
        ]);

        \App\Models\User::factory()->create([
            'name' => 'buyer',
            'email' => 'buyer@gmail.com',
            'role' => 'user',
        ]);

        $price1 = 123.45*100;
        $price2 = 45.65*100;
        $price3 = 39.73*100;
        $price4 = 250.00*100;
        $price5 = 59.35*100;
        Product::create([
            'name' => 'Product 1',
            'price' => 123.45*100,
            'tax' => 5,
        ]);
        Product::create([
            'name' => 'Product 2',
            'price' => 45.65*100,
            'tax' => 15,
        ]);
        Product::create([
            'name' => 'Product 3',
            'price' => 39.73*100,
            'tax' => 12,
        ]);
        Product::create([
            'name' => 'Product 4',
            'price' => 250.00*100,
            'tax' => 8,
        ]);
        Product::create([
            'name' => 'Product 5',
            'price' => 59.35*100,
            'tax' => 10,
        ]);


    }
}
