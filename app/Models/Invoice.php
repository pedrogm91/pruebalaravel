<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;

    protected $fillable = [
       'products_details',
       'tax_total',
       'total',
       'shopping_id'
    ];

    public function shopping()
    {
        return $this->belongsTo(Shopping::class);
    }
}
