<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Shopping;
use Illuminate\Auth\Access\HandlesAuthorization;

class ShoppingPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function view(User $user, Shopping $shopping)
    {
        return $user->role === 'admin'
                ? true
                : false;
    }

}
