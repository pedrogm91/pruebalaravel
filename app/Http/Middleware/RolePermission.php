<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class RolePermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$roles)
    {
        $role_user_auth = $request->user()->role;
        $collection_roles = collect($roles);
        
        if (!$collection_roles->contains($role_user_auth)) {
            return redirect('dashboard')->with('error', 'sin permisos para acceder');
        }
 
        return $next($request);
    }
}
