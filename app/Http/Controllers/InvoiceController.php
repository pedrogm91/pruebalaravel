<?php

namespace App\Http\Controllers;

use App\Models\Shopping;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $current_user = auth()->user()->id;
        
        if (auth()->user()->role === 'admin') {
            $invoices = Shopping::where('status', 'close')->with('invoice')->orderBy('id', 'desc')->get()->pluck('invoice');
            return view('invoices.index', ['invoices' => $invoices]);
        }

        $invoices = Shopping::where('status', 'close')->where('user_id', $current_user)->with('invoice')->orderBy('id', 'desc')->get()->pluck('invoice');
        return view('invoices.index', ['invoices' => $invoices]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $shopping_id = $request->id;
        $shopping = Shopping::findOrFail($shopping_id)->load(['products']);
        $products = collect($shopping->only('products'))->collapse();

        // Array de la lista de productos con name | price | tax | subtotal
        $products_details = $products->map(function($item, $key) {
            $subtotal = $item->price / 100;
            $tax = $subtotal * $item->tax / 100;
            $array = [
                'name' => $item->name,
                'price' => number_format($subtotal - $tax, 2),
                'tax' =>  $item->tax."%", 
                'subtotal' =>  number_format($subtotal, 2),
            ];
            return $array;
        })->toJson();
        
        // Total Tax
        $sumTax = $products->map(function($item, $key) {
            $subtotal = $item->price;
            $tax = $subtotal * $item->tax / 100;
            return $tax;
        })->sum();
        $taxTotal = number_format($sumTax / 100, 2); // Suma del tax de todos los productos
        
        // Total
        $sum = $products->map(function($item, $key) {
            return $item->price;
        })->sum();
        $total = number_format($sum / 100, 2); // Suma del total del monto de la factura

        // save()
        $invoice = new Invoice([
            'products_details' => $products_details,
            'tax_total' => $taxTotal,
            'total' => $total,
            'shopping_id' => $shopping_id
        ]);

        $invoice->save();

        DB::table('shoppings')
              ->where('id', $shopping_id)
              ->update(['status' => 'close']);

        return redirect()->route('invoices.index')->with('success', 'Factura generada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        $details = json_decode($invoice->products_details, true);
        
        return view('invoices.show', [
            'invoice' => $invoice,
            'details' => collect($details)
        ]);
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        //
    }

    public function storeAll()
    {
        $data = Shopping::where('status', 'open')->get();
        foreach ($data as $key => $item) {
            $shopping = Shopping::findOrFail($item->id)->load(['products']);
            $products = collect($shopping->only('products'))->collapse();

            // Array de la lista de productos con name | price | tax | subtotal
            $products_details = $products->map(function($item, $key) {
                $subtotal = $item->price / 100;
                $tax = $subtotal * $item->tax / 100;
                $array = [
                    'name' => $item->name,
                    'price' => number_format($subtotal - $tax, 2),
                    'tax' =>  $item->tax."%", 
                    'subtotal' =>  number_format($subtotal, 2),
                ];
                return $array;
            })->toJson();
            
            // Total Tax
            $sumTax = $products->map(function($item, $key) {
                $subtotal = $item->price;
                $tax = $subtotal * $item->tax / 100;
                return $tax;
            })->sum();
            $taxTotal = number_format($sumTax / 100, 2); // Suma del tax de todos los productos
            
            // Total
            $sum = $products->map(function($item, $key) {
                return $item->price;
            })->sum();
            $total = number_format($sum / 100, 2); // Suma del total del monto de la factura

            // save()
            $invoice = new Invoice([
                'products_details' => $products_details,
                'tax_total' => $taxTotal,
                'total' => $total,
                'shopping_id' => $item->id
            ]);

            $invoice->save();

            DB::table('shoppings')
                ->where('id', $item->id)
                ->update(['status' => 'close']);

        }
        return redirect()->route('invoices.index')->with('success', 'Facturas generadas');
    }
}
