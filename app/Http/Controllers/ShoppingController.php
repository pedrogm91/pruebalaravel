<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Product;
use App\Models\Shopping;
use Illuminate\Http\Request;

class ShoppingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Shopping $shopping)
    {
        $this->authorize('view', $shopping);
        $shoppings = Shopping::where('status', 'open')->get();
        return view('shoppings.index', ['shoppings' => $shoppings]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::findOrFail(auth()->user()->id);
        $date = date("h:i:sa");
        $product = $request->id;
        $shopping = Shopping::where('user_id', $user->id)->where('status', 'open')->get();
        if ($shopping->isEmpty()) {
            $newShopping = new Shopping([
                'name' => "$user->name $date",
                'status' => 'open',
            ]);
            
            $user->shoppings()->save($newShopping);
            
            $newShopping->products()->syncWithoutDetaching([$product]);
            
            $newShopping->save();
        }
        
        if ($shopping->isNotEmpty()) {
            $shopping->last()->products()->syncWithoutDetaching([$product]);
        }

        return redirect()->route('products.index')->with('success', 'Agregado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shopping  $shopping
     * @return \Illuminate\Http\Response
     */
    public function show(Shopping $shopping)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shopping  $shopping
     * @return \Illuminate\Http\Response
     */
    public function edit(Shopping $shopping)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Shopping  $shopping
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Shopping $shopping)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shopping  $shopping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shopping $shopping)
    {
        //
    }
}
